package andriod.romon.reddit;

import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.FloatMath;

import java.util.List;

public class RedditWebActivity extends AppCompatActivity{

    private ViewPager viewPager;
    private List<RedditPost> redditPosts;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private float acceleration;
    private float currentAcceleration;
    private float previousAcceleration;
    private FragmentManager fragmentManager;
    private Uri redditUri;


    private final SensorEventListener sensorListener = new SensorEventListener() {
        @Override
        public void onSensorChanged(SensorEvent event) {
            float x = event.values[0];
            float y = event.values[1];
            float z = event.values[2];

            previousAcceleration = currentAcceleration;
            currentAcceleration = (float)Math.sqrt(x * x + y * y + z * z);
            float delta = currentAcceleration - previousAcceleration;
            acceleration = acceleration * 0.9f + delta;

            if(acceleration > 20) {
                final int randomNumber = (int) (Math.random() * 29);
                viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
                    @Override
                    public Fragment getItem(int position) {
                        RedditPost redditPost = redditPosts.get(position + 1);
                        return RedditWebFragment.newFragment(redditPost.url);
                    }

                    @Override
                    public int getCount() {
                        return redditPosts.size();
                    }

                });

                viewPager.setCurrentItem(randomNumber);

            }
            else if(acceleration > 10){
                FragmentManager fragmetManager = getSupportFragmentManager();
                viewPager.setAdapter(new FragmentStatePagerAdapter(fragmetManager) {
                    @Override
                    public Fragment getItem(int position) {
                        RedditPost redditPost = redditPosts.get(position + 1);
                        return RedditWebFragment.newFragment(redditPost.url);
                    }

                    @Override
                    public int getCount() {
                        return redditPosts.size();
                    }
                });

                viewPager.setCurrentItem(viewPager.getCurrentItem() + 1);
            }

        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reddit_web);
        viewPager = (ViewPager)findViewById(R.id.viewPager);
        redditPosts = RedditPostParser.getInstance().redditPosts;

        Intent intent = getIntent();
        Uri redditUri = intent.getData();

        sensorManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        acceleration = 0.0f;
        currentAcceleration = SensorManager.GRAVITY_EARTH;
        previousAcceleration = SensorManager.GRAVITY_EARTH;

        FragmentManager fragmetManager = getSupportFragmentManager();
        viewPager.setAdapter(new FragmentStatePagerAdapter(fragmetManager) {
            @Override
            public Fragment getItem(int position) {
                RedditPost redditPost = redditPosts.get(position);
                return RedditWebFragment.newFragment(redditPost.url);
            }

            @Override
            public int getCount() {
                return redditPosts.size();
            }
        });

        for(int index= 0; index < redditPosts.size(); index++) {
            if(redditPosts.get(index).url.equals(redditUri.toString())) {
                viewPager.setCurrentItem(index);
                break;
            }
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(sensorListener);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(sensorListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
}
